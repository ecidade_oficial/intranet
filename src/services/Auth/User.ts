import { axiosInstance } from '../AxiosInstance';
import { URL } from '../../helpers/constants/URL';
import { HttpCodes } from '../../helpers/constants/HttpCodes';

export const getUser = async () => {
    const response = await axiosInstance.get(`${URL.baseUrl}${URL.user.getUser}`);
    if (response.status === HttpCodes.SUCCESS) {
        sessionStorage.setItem('@App:user', JSON.stringify(response.data));
    }
}