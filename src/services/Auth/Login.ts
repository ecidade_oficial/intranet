import { axiosInstance } from './../AxiosInstance';
import { getUser } from './User';
import { cookieName } from './../../helpers/constants/Names';
import { URL } from '../../helpers/constants/URL';
import { HttpCodes } from '../../helpers/constants/HttpCodes';
import CookieAdapter from '../Cookie/CookieAdapter';


type IProp = {
    user: string;
    password: string;
}

export const Login = async ({ user, password }: IProp): Promise<any> => {

    try {
        const response = await axiosInstance.post(`${URL.baseUrl}${URL.auth.login}`, { email: user, password });

        if (response.status === HttpCodes.SUCCESS) {
            axiosInstance.interceptors.request.use(function (config) {
                config.headers.Authorization = `Bearer ${response.data.token}`;
                return config;
            });
            const cookie = new CookieAdapter();
            cookie.create(cookieName, response.data.token);
            await getUser();
        }
        return {
            success: true,
            msg: 'Acesso concedido'
        };
    } catch (err: any) {
        console.warn(err)
        return {
            success: false,
            msg: err.response.data
        };
    }
}

export const Logout = async () => {

    sessionStorage.removeItem('@App:user')

    const cookie = new CookieAdapter();
    cookie.destroy(cookieName);
    await axiosInstance.post(`${URL.baseUrl}${URL.auth.logout}`);

}
