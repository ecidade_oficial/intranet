export interface CookieInterface {
    create: (tokenName: string, token: string) => void;
    getCookie: (tokenName: string) => any;
    destroy: (tokenName: string) => void;
}