import { CookieInterface } from './CookieInterface';
import { setCookie, getCookie, removeCookie } from "react-simple-cookie-store";

class CookieAdapter implements CookieInterface {
    create(tokenName: string, token: string) {
        setCookie(tokenName, token, 1);
    }
    getCookie(tokenName: string) {
        return getCookie(tokenName);
    }
    destroy(tokenName: string) {
        removeCookie(tokenName);
    }
}

export default CookieAdapter;