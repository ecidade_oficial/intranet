import { axiosInstance } from './AxiosInstance';
import axios from "axios";

export type Address = {
    "cep": string;
    "logradouro": string;
    "complemento": string;
    "bairro": string;
    "localidade": string;
    "uf": string;
    "ibge": string;
    "gia": string;
    "ddd": string;
    "siafi": string;
}
export type Error = {
    error:boolean;
}
export const consultZipcode = async (zipcode:string) : Promise<Address|Error> => {
    const response = await axios.get(`https://viacep.com.br/ws/${zipcode}/json/unicode/`);
    return response.data;


}
