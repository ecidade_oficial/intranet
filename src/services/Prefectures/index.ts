import Prefecture, { PrefectureInterface } from "@/model/Prefecture";

import { axiosInstance } from './../AxiosInstance';
import { URL } from '../../helpers/constants/URL';
import { plainToClass } from 'class-transformer';
import { toast } from "react-toastify";
export const getAllPrefectures = async (): Promise<any> => {
    try {
        const prefectures = await axiosInstance.get(`${URL.baseUrl}${URL.prefecture}`);

        let jsonObject = prefectures.data as Object;
        let convertPrefectures = plainToClass(Prefecture, jsonObject);
        return convertPrefectures;
    } catch (error) {
        toast.error(`🦄 Ocorreu um problema interno ao obter a lista de prefeituras`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}
export const getPrefecture = async (prefectureId: number): Promise<any> => {
    try {
        const prefectures = await axiosInstance.get(`${URL.baseUrl}${URL.prefecture}/${prefectureId}`);

        let jsonObject = prefectures.data as Object;
        let convertPrefectures = plainToClass(Prefecture, jsonObject);
        return convertPrefectures;
    } catch (error) {
        toast.error(`🦄 Ocorreu um problema interno ao obter a prefeitura`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}
export const create = async (prefecture: PrefectureInterface): Promise<any> => {
    try {
        const response = await axiosInstance.post(`${URL.baseUrl}${URL.prefecture}`, JSON.stringify(prefecture));
        toast.success(`👌🤘 A ${prefecture.name} foi cadastrada com sucesso`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        return response;
    } catch (error) {
        toast.error(`👌🤘 Ocorreu um problema interno ao obter a cadastrar a prefeitura ${prefecture.name}`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}
export const update = async (prefecture: PrefectureInterface, prefectureId: number): Promise<any> => {
    try {
        const response = await axiosInstance.put(`${URL.baseUrl}${URL.prefecture}/${prefectureId}`, JSON.stringify(prefecture));
        toast.success(`👌 A ${prefecture.name} foi atualizada com sucesso`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        return response;
    } catch (error) {
        toast.error(`🦄 Ocorreu um problema interno ao obter ao atualizar a prefeitura ${prefecture.name}`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}