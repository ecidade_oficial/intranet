import { cookieName } from "@/helpers/constants/Names";
import { axiosInstance } from "@/services/AxiosInstance";
import CookieAdapter from "@/services/Cookie/CookieAdapter";
import React, { createContext, useContext, useEffect, useState } from "react";
import {
  Login as LoginService,
  Logout as LogoutService,
} from "../services/Auth/Login";
type UserProp = {
  name: string;
  document: string;
  email: string;
  owner_id: number;
};
type AuthContextData = {
  signed: boolean;
  user: UserProp | null;
  Authenticate(user: object): Promise<any>;
  Logout(): void;
  isLoading: boolean;
};
type IProp = {
  user: string;
  password: string;
};

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<UserProp | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    setIsLoading(true);
    const storagedUser = sessionStorage.getItem("@App:user");
    const cookie = new CookieAdapter();
    const storagedToken = cookie.getCookie(cookieName);
    if (storagedUser && storagedToken) {
      setUser(JSON.parse(storagedUser));
      axiosInstance.interceptors.request.use(function (config) {
        config.headers.Authorization = `Bearer ${storagedToken}`;
        return config;
      });
    }
    setIsLoading(false);
  }, []);

  async function Authenticate(userData: IProp): Promise<any> {
    const response = await LoginService(userData);
    if (response.success) {
      const storagedUser = sessionStorage.getItem("@App:user");
      if (storagedUser) {
        setUser(JSON.parse(storagedUser));
        const cookie = new CookieAdapter();
        const storagedToken = cookie.getCookie(cookieName);
        axiosInstance.interceptors.request.use(function (config) {
          config.headers.Authorization = `Bearer ${storagedToken}`;
          return config;
        });
      }
    }
    return response;
  }

  function Logout() {
    setUser(null);
    LogoutService();
  }

  return (
    <AuthContext.Provider
      value={{ signed: Boolean(user), user, Authenticate, Logout, isLoading }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);

  return context;
}
