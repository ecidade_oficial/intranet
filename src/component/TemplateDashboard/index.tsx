import React, { ReactChildren } from "react";

import SideBarNavDesktop from "../SideBarNavDesktop";
import SideBarNavMobile from "../SideBarNavMobile";

type IProps = {
  children: JSX.Element | JSX.Element[];
};

export default function TemplateDashboard({ children }: IProps) {
  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <SideBarNavMobile />
      {/* Static sidebar for desktop */}
      <SideBarNavDesktop />
      <div className="flex flex-col w-0 flex-1 overflow-hidden">
        <main className="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div className="py-6">{children}</div>
        </main>
      </div>
    </div>
  );
}
