import React, { InputHTMLAttributes } from "react";
import { ExclamationCircleIcon } from "@heroicons/react/outline";
import { DeepMap, FieldError, UseFormRegister } from "react-hook-form";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  register: UseFormRegister<any>;
  errors: DeepMap<any, FieldError>;
  label: string;
  inputName: string;
  messageError?: string;
  type?: string;
  required?: boolean;
  disabled?: boolean;
}

export default function Input({
  register,
  errors,
  label,
  inputName,
  messageError,
  type = "text",
  required = false,
  disabled = false,
  ...props
}: InputProps) {
  return (
    <>
      <div className=" rounded-md shadow-sm">
        <label
          htmlFor={inputName}
          className="block text-sm font-medium text-gray-700"
        >
          {label}{" "}
          {required && <code className="text-red-600 font-bold">*</code>}
        </label>
        <input
          {...register(
            `${inputName}`,
            required ? { required: messageError } : {}
          )}
          id={inputName}
          name={inputName}
          type={type}
          autoComplete="off"
          disabled={disabled}
          className={`appearance-none block w-full ${
            type !== "color" && "px-3 py-2"
          } border  rounded-md shadow-sm focus:outline-none ${
            errors[inputName]
              ? "border-red-300 text-red-900 placeholder-red-300  focus:ring-red-500"
              : "border-gray-300  placeholder-gray-400 focus:ring-indigo-500 focus:border-indigo-500"
          }  sm:text-sm ${disabled ? "bg-gray-100" : ""} ${
            required && "border-gray-900"
          }`}
          {...props}
        />

        {errors[inputName] && (
          <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
            <ExclamationCircleIcon
              className="h-5 w-5 text-red-500"
              aria-hidden="true"
            />
          </div>
        )}
      </div>
      {errors[inputName] && (
        <p className="mt-2 text-sm text-red-600">{errors[inputName].message}</p>
      )}
    </>
  );
}
