import React from "react";
import { DeepMap, FieldError, UseFormRegister } from "react-hook-form";
import { Link } from "react-router-dom";
import Prefecture, { PrefectureInterface } from "../../model/Prefecture";
import Input from "../Input";
interface CreatePrefectureProps {
  register: UseFormRegister<Prefecture>;
  errors: DeepMap<Prefecture, FieldError>;
  prefecture?: Prefecture;
}
function FormPrefeitura({
  register,
  errors,
  prefecture,
}: CreatePrefectureProps) {
  return (
    <>
      <div className=" mx-auto px-4 sm:px-6 md:px-8">
        <div className="py-4">
          <div className="bg-white shadow px-4 py-5 sm:rounded-lg sm:p-6">
            <div className="pb-5">
              <div className="py-4">
                <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                  <div className="sm:col-span-1  md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Prefeitura"
                      inputName="name"
                      messageError="Informe o Nome da Prefeitura"
                      required={true}
                    />
                  </div>
                  <div className="sm:col-span-1  md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Dominio"
                      inputName="prefixDomain"
                      messageError="Informe o Dominio"
                      required={true}
                    />
                  </div>
                  <div className="sm:col-span-2  md:col-span-3 lg:col-span-2  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="OneSignal APP ID"
                      inputName="oneSignalAppId"
                      required={false}
                    />
                  </div>
                  <div className="sm:col-span-2   md:col-span-3 lg:col-span-2 relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="OneSignal API KEY"
                      inputName="oneSignalApiKey"
                      required={false}
                    />
                  </div>
                </div>
                <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                  <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="OneSignal Project Number"
                      inputName="configurations.oneSignalProjectNumber"
                      required={false}
                    />
                  </div>
                  <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Chave Firebase"
                      inputName="configurations.firebaseServerKey"
                      required={false}
                    />
                  </div>
                  <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="URL Loja Android"
                      inputName="urlAndroid"
                      required={false}
                    />
                  </div>
                  <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="URL Loja IOS"
                      inputName="urlIos"
                      required={false}
                    />
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Apple Login"
                      inputName="configurations.appleLogin"
                      required={false}
                    />
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Apple Senha"
                      inputName="configurations.applePassword"
                      required={false}
                    />
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Código HG"
                      inputName="hgBrasilName"
                      required={false}
                    />
                    <Link
                      to={{
                        pathname:
                          "https://console.hgbrasil.com/documentation/weather/tools",
                      }}
                      title="Obter Código HD"
                      target="_blank"
                      className="underline text-gray-800 font-medium"
                    >
                      <small>Obter Código</small>
                    </Link>
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Usuário"
                      inputName="user.email"
                      required={true}
                      messageError="Informe o E-mail"
                      type="email"
                    />
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Senha"
                      inputName="user.password"
                      messageError="Informe a Senha"
                      required={true}
                      type="password"
                    />
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Cor do Modulo"
                      inputName="configurations.backgroundColor"
                      messageError="Informe a cor dos modulos"
                      required={true}
                    />
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Cor Titulo Modulo"
                      inputName="configurations.titleColor"
                      required={true}
                      messageError="Informe a Cor do Titulo"
                    />
                  </div>
                  <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                    <Input
                      register={register}
                      errors={errors}
                      label="Cor Texto Modulo"
                      inputName="configurations.textColor"
                      required={true}
                      messageError="Informe a Cor do Modulo"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-between mx-auto px-4 sm:px-6 md:px-8">
        <Link
          to="/prefeituras"
          className="inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Voltar
        </Link>
        <button
          type="submit"
          className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Salvar
        </button>
      </div>
    </>
  );
}

export default FormPrefeitura;
