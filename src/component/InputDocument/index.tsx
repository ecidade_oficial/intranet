import React,{ createElement,useState } from 'react';
import {DeepMap, FieldError, useForm, UseFormRegister} from 'react-hook-form';
import {cpfMask} from '../../helpers/mask';
import Input from "../Input";
import {ExclamationCircleIcon} from "@heroicons/react/outline";

type InputProps = {
    register: UseFormRegister<any>;
    errors: DeepMap<any, FieldError>;
}

const InputDocument = ({register,errors}: InputProps) => {
    const [document,setDocument] = useState('');
    const handleChange = (e:any) => {
        setDocument(cpfMask(e.target.value));
    }
    return  <>
        <div className=" rounded-md shadow-sm">
            <label
                htmlFor='document'
                className="block text-sm font-medium text-gray-700"
            >
                {" "}
                Document
            </label>
            <input
                {...register('document', { required: "Informe o documento" })}
                id='document'
                name='document'
                value={document}

                onChange={handleChange}
                type="text"
                className={`appearance-none block w-full px-3 py-2 border  rounded-md shadow-sm focus:outline-none ${
                    errors.document
                        ? "border-red-300 text-red-900 placeholder-red-300  focus:ring-red-500"
                        : "border-gray-300  placeholder-gray-400 focus:ring-indigo-500 focus:border-indigo-500"
                }  sm:text-sm`}
            />
            {errors.document && (
                <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <ExclamationCircleIcon
                        className="h-5 w-5 text-red-500"
                        aria-hidden="true"
                    />
                </div>
            )}
        </div>
        {errors.document && (
            <p className="mt-2 text-sm text-red-600">
                {errors.document.message}
            </p>
        )}
    </>
}

export default InputDocument;
