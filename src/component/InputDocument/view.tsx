import React from "react";
import { DeepMap, FieldError, UseFormRegister } from "react-hook-form";
type IProps = {
  handleChange: (e: any) => void;
  document: string;
  isRequired: boolean;
  register: UseFormRegister<any>;
  errors: DeepMap<any, FieldError>;
};

const View = ({
  handleChange,
  document,
  isRequired,
  register,
  errors,
}: IProps) => {
  return (
    <>
          <input
            {...register("document", { required: true })}
            id="document"
            name="document"
            type="text"
            value={document}

            onChange={handleChange}
            className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
          {errors.document && <span>Campo Obrigatório</span>}
        </>
  );
};

export default View;
