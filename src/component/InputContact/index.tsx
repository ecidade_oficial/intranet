import React, {useState} from 'react';
import {ExclamationCircleIcon} from "@heroicons/react/outline";
import {DeepMap, FieldError, UseFormRegister} from "react-hook-form";
import {phoneMask} from "../../helpers/mask";

type InputProps = {
    register: UseFormRegister<any>;
    errors: DeepMap<any, FieldError>;
    label: string;
    inputName: string;
    messageError?: string;
    type?: string;
    required?: boolean;
}

export default function InputContact({
                                  register,
                                  errors,
                                  label,
                                  inputName,
                                  messageError,
                                  type = "text",
                                  required = false
                              }: InputProps) {
    const [contact,setContact] = useState('');
    const handleChange = (e:any) => {
        setContact(phoneMask(e.target.value));
    }
    return (
        <>
            <div className=" rounded-md shadow-sm">
                <label
                    htmlFor={inputName}
                    className="block text-sm font-medium text-gray-700"
                >
                    {" "}
                    {label}
                </label>
                <input
                    {...register(`${inputName}`, required ? {required: messageError} : {})}
                    id={inputName}
                    name={inputName}
                    type={type}
                    autoComplete="off"
                    className={`appearance-none block w-full px-3 py-2 border  rounded-md shadow-sm focus:outline-none ${
                        errors[inputName]
                            ? "border-red-300 text-red-900 placeholder-red-300  focus:ring-red-500"
                            : "border-gray-300  placeholder-gray-400 focus:ring-indigo-500 focus:border-indigo-500"
                    }  sm:text-sm`}
                    value={contact}
                    onChange={handleChange}
                />
                {errors[inputName] && (
                    <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                        <ExclamationCircleIcon
                            className="h-5 w-5 text-red-500"
                            aria-hidden="true"
                        />
                    </div>
                )}
            </div>
            {errors[inputName] && (
                <p className="mt-2 text-sm text-red-600">
                    {errors[inputName].message}
                </p>
            )}
        </>
    );
}
