import React from "react";
import { useAuth } from "../../contexts/auth";

export default function Avatar() {
  const { user, Logout } = useAuth();

  return (
    <div className="ml-3">
      <p className="text-sm font-medium text-gray-700 group-hover:text-gray-900">
        {user?.name}
      </p>

      <span
        onClick={() => Logout()}
        className="text-xs font-medium text-gray-500 group-hover:text-gray-700 cursor-pointer"
      >
        Sair
      </span>
    </div>
  );
}
