import React from 'react';
import { useAuth } from '../../contexts/auth';


export default function Logotipo(){
    const {user} = useAuth()
    return ( <span className="inline-flex items-center justify-center h-8 w-8 rounded-full bg-blue-400">
    <span className="text-xs font-medium leading-none text-white"></span>
  </span>);
}
