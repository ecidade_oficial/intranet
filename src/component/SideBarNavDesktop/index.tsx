import React from "react";
import { Link } from "react-router-dom";
import Avatar from "../Avatar";

import { BriefcaseIcon, HomeIcon } from "@heroicons/react/outline";

export default function SideBarNavDesktop() {
  return (
    <div className="hidden md:flex md:flex-shrink-0">
      <div className="flex flex-col w-64">
        <div className="flex flex-col h-0 flex-1 border-r border-gray-200 bg-white">
          <div className="flex-1 flex flex-col pt-5 pb-4 overflow-y-auto">
            <div className="flex-shrink-0 flex items-center px-4">
              <img
                className="w-24  mx-auto"
                src="https://ecidade.app/assets/img/eCIDADE_logo.png"
                alt="eCidade Intranet"
              />
            </div>
            <nav className="mt-5 flex-1 px-2 bg-white space-y-1">
              <Link
                to="/painel"
                className="text-gray-600 hover:bg-gray-50 hover:text-gray-900group flex items-center px-2 py-2 text-sm font-medium rounded-md"
              >
                <HomeIcon
                  className="text-gray-400 group-hover:text-gray-50 mr-3 flex-shrink-0 h-6 w-6"
                  aria-hidden="true"
                />
                Página Inicial
              </Link>
              <Link
                to="/prefeituras"
                className="text-gray-600 hover:bg-gray-50 hover:text-gray-900group flex items-center px-2 py-2 text-sm font-medium rounded-md"
              >
                <BriefcaseIcon
                  className="text-gray-400 group-hover:text-gray-50 mr-3 flex-shrink-0 h-6 w-6"
                  aria-hidden="true"
                />
                Prefeituras
              </Link>
            </nav>
          </div>
          <div className="flex-shrink-0 flex border-t border-gray-200 p-4">
            <a href="#" className="flex-shrink-0 w-full group block">
              <div className="flex items-center">
                <Avatar />
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
