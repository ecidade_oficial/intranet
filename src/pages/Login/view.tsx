import React from "react";

import {
  DeepMap,
  FieldError,
  SubmitHandler,
  UseFormHandleSubmit,
  UseFormRegister,
} from "react-hook-form";

import { LoginForm } from ".";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { ExclamationCircleIcon } from "@heroicons/react/outline";
import { ToastContainer } from "react-toastify";
import Botao from "@/component/Botao";
type IProps = {
  handleLogin: (data: LoginForm) => Promise<void>;
  handleSubmit: UseFormHandleSubmit<LoginForm>;
  register: UseFormRegister<LoginForm>;
  errors: DeepMap<LoginForm, FieldError>;
  handleChange: (e: any) => void;
  user: string;
  loading: boolean;
};
const View = ({
  handleLogin,
  handleSubmit,
  register,
  errors,
  handleChange,
  user,
  loading,
}: IProps) => {
  return (
    <HelmetProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>eCidade Intranet | Acesso ao sistema</title>
      </Helmet>

      <div className="min-h-screen bg-gray-50 flex flex-col justify-center py-12 sm:px-6 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <img
            className="mx-auto h-auto w-32"
            src="https://ecidade.app/assets/img/eCIDADE_logo.png"
            alt="eCidade Intranet"
          />
        </div>

        <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
          <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
            <form
              className="space-y-6"
              onSubmit={handleSubmit(handleLogin)}
              method="POST"
            >
              <div>
                <label
                  htmlFor="user"
                  className="block text-sm font-medium text-gray-700"
                >
                  {" "}
                  Usuário
                </label>
                <div className="mt-1 relative rounded-md shadow-sm">
                  <input
                    {...register("user", { required: true })}
                    id="user"
                    name="user"
                    type="text"
                    value={user}
                    onChange={handleChange}
                    className={`appearance-none block w-full px-3 py-2 border  rounded-md shadow-sm focus:outline-none ${
                      errors.user
                        ? "border-red-300 text-red-900 placeholder-red-300  focus:ring-red-500"
                        : "border-gray-300  placeholder-gray-400 focus:ring-indigo-500 focus:border-indigo-500"
                    }  sm:text-sm`}
                  />
                  {errors.user && (
                    <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                      <ExclamationCircleIcon
                        className="h-5 w-5 text-red-500"
                        aria-hidden="true"
                      />
                    </div>
                  )}
                </div>
                {errors.user && (
                  <p className="mt-2 text-sm text-red-600">Campo Obrigatório</p>
                )}
              </div>

              <div>
                <label
                  htmlFor="password"
                  className="block text-sm font-medium text-gray-700"
                >
                  Senha
                </label>
                <div className="mt-1  relative rounded-md shadow-sm">
                  <input
                    {...register("password", { required: true })}
                    id="password"
                    name="password"
                    type="password"
                    className={`appearance-none block w-full px-3 py-2 border  rounded-md shadow-sm focus:outline-none ${
                      errors.password
                        ? "border-red-300 text-red-900 placeholder-red-300  focus:ring-red-500"
                        : "border-gray-300  placeholder-gray-400 focus:ring-indigo-500 focus:border-indigo-500"
                    }  sm:text-sm`}
                  />
                  {errors.user && (
                    <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                      <ExclamationCircleIcon
                        className="h-5 w-5 text-red-500"
                        aria-hidden="true"
                      />
                    </div>
                  )}
                </div>
                {errors.user && (
                  <p className="mt-2 text-sm text-red-600">Campo Obrigatório</p>
                )}
              </div>
              <div>
                <Botao
                  loading={loading}
                  titleButton="Entrar"
                  titleLoading="Entrando"
                  customClass="w-full"
                />
              </div>
            </form>
          </div>
        </div>
      </div>
      <ToastContainer />
    </HelmetProvider>
  );
};

export default View;
