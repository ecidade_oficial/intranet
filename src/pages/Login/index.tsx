import { createElement, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { useAuth } from "@/contexts/auth";
import View from "./view";
import { toast } from "react-toastify";

export type LoginForm = {
  user: string;
  password: string;
};

const Login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginForm>();
  const [user, setUser] = useState("");
  const [loading, setLoading] = useState(false);
  const { Authenticate, signed } = useAuth();
  const history = useHistory();
  const handleLogin = async (data: LoginForm) => {
    setLoading(true);
    let response = await Authenticate(data);
    if (!response.success) {
      setLoading(false);
      toast.error(`🦄 ${response.msg.errors}`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      toast.success(`👐 Acesso concedido`, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      setTimeout(() => {
        setLoading(false);

        history.push("/painel");
      }, 3000);
    }
  };

  const handleChange = (e: any) => {
    setUser(e.target.value);
  };
  return createElement(View, {
    handleLogin,
    handleSubmit,
    register,

    errors,
    handleChange,
    user,
    loading,
  });
};

export default Login;
