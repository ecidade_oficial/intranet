import React, { createElement, useState } from "react";
import View from "./view";
import { useForm } from "react-hook-form";
import Prefecture, { PrefectureInterface } from "@/model/Prefecture";
import { HttpCodes } from "@/helpers/constants/HttpCodes";
import { useHistory } from "react-router-dom";
import slugify from "slugify";

export default function CreatePrefecture() {
  const [loading, setLoading] = useState(false);
  const {
    register,
    handleSubmit,
    getValues,
    setValue,
    formState: { errors },
  } = useForm<PrefectureInterface>();
  const history = useHistory();
  const handleGenerateSlug = (): void => {
    let values = getValues();
    let name = values.name;
    setValue("prefixDomain", slugify(name, { replacement: "_", lower: true }));
  };
  const handleRegister = async (data: PrefectureInterface): Promise<any> => {
    setLoading(true);
    const newPrefecture = await Prefecture.createPrefecture(data);

    if (newPrefecture.status === HttpCodes.CREATED) {
      setLoading(false);
      history.push("/prefeituras");
    }
  };
  return createElement(View, {
    register,
    errors,
    handleSubmit,
    handleRegister,
    handleGenerateSlug,
    loading,
  });
}
