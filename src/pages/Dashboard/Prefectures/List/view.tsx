import React, { useState } from "react";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { Link } from "react-router-dom";
import Slide from "@/component/Slide";
import Prefecture, { PrefectureInterface } from "@/model/Prefecture";
interface ListPrefectureProps {
  prefectures?: PrefectureInterface[];
}
const View = ({ prefectures }: ListPrefectureProps) => {
  const [open, setOpen] = useState(false);
  const [prefecture, setPrefecture] = useState<PrefectureInterface>(
    {} as PrefectureInterface
  );
  const handleShowSlide = (prefect: any) => {
    setPrefecture(prefect);
    setOpen(true);
  };
  const handleCloseSlide = () => {
    setOpen(false);
  };

  return (
    <HelmetProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>eCidade Intranet | Listagem Prefeituras</title>
      </Helmet>
      <div className="flex justify-between mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900">Prefeituras</h1>
        <Link
          to="/prefeituras/novo"
          className="inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Novo
        </Link>
      </div>
      <div className=" mx-auto px-4 sm:px-6 md:px-8">
        <div className="py-4">
          <div className="bg-white shadow overflow-hidden sm:rounded-md">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                  <table className="min-w-full divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Nome
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Dominio
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Status
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      {prefectures ? (
                        prefectures.map((prefectureItem) => {
                          return (
                            <tr key={prefectureItem.prefix}>
                              <td className="px-6 py-4 whitespace-nowrap">
                                <div className="flex items-center">
                                  <div className="flex-shrink-0 h-10 w-10">
                                    <img
                                      className="h-10 w-10 rounded-full"
                                      src={
                                        prefectureItem.logo !== null
                                          ? prefectureItem.logo
                                          : "https://via.placeholder.com/150"
                                      }
                                      alt={prefectureItem.name}
                                    />
                                  </div>
                                  <div className="ml-4">
                                    <div
                                      className="text-sm font-medium text-gray-900 cursor-pointer"
                                      onClick={() =>
                                        handleShowSlide(prefectureItem)
                                      }
                                    >
                                      {prefectureItem.name}
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap">
                                <div className="text-sm text-gray-900">
                                  <Link
                                    to={{
                                      pathname: `https://${prefectureItem.prefix}.ecidade.app`,
                                    }}
                                    target="_blank"
                                  >
                                    <span className="bg-white border border-green-500 p-2 rounded-md hover:bg-green-500 hover:text-white">
                                      {prefectureItem.status}
                                    </span>
                                  </Link>
                                </div>
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap">
                                <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                  Ativo
                                </span>
                              </td>
                            </tr>
                          );
                        })
                      ) : (
                        <tr>
                          <td colSpan={4}>Nenhuma Prefeitura Cadastrada</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Slide
        title={`${prefecture.name}`}
        logo={`${prefecture.logo}`}
        openSlide={open}
        closeSlide={handleCloseSlide}
      >
        <>
          <p>
            <strong className="text-indigo-500 text-sm font-bold">
              Dominio
            </strong>
            <br />
            <span className="text-sm font-medium">
              https://{prefecture.prefix}
              .ecidade.app
            </span>
          </p>
          <p className="mt-2">
            <strong className="text-indigo-500 text-sm font-bold">
              OneSignal App ID
            </strong>
            <br />
            <span className="text-xs bg-yellow-200 p-1 rounded-md">
              {prefecture.oneSignalAppId}
            </span>
          </p>
          <p>
            <strong className="text-indigo-500 text-sm font-bold">
              OneSignal Api KEY
            </strong>
            <br />
            <span className="text-xs bg-yellow-200 p-1 rounded-md">
              {prefecture.onesignalApiId}
            </span>
          </p>
          <h3 className="mt-4 font-bold text-lg">Configurações</h3>
          <div className="flex w-full">
            <p>
              <strong className="text-indigo-500 text-sm font-bold mr-24">
                Apple Login
              </strong>
              <br />

              <span className="text-xs bg-yellow-200 p-1 rounded-md">
                {prefecture.configurations?.appleLogin
                  ? prefecture.configurations?.appleLogin
                  : "Não Cadastrado"}
              </span>
            </p>
            <p>
              <strong className="text-indigo-500 text-sm font-bold">
                Apple Senha
              </strong>
              <br />

              <span className="text-xs bg-yellow-200 p-1 rounded-md">
                {prefecture.configurations?.applePassword
                  ? prefecture.configurations?.applePassword
                  : "Não Cadastrado"}
              </span>
            </p>
          </div>
          <div className="flex w-full mt-14">
            <p>
              <strong className="text-indigo-500 text-sm font-bold mr-24">
                Chave Firebase
              </strong>
              <br />

              <span className="text-xs bg-yellow-200 p-1 rounded-md">
                {prefecture.configurations?.firebaseServerKey
                  ? prefecture.configurations?.firebaseServerKey
                  : "Não Cadastrado"}
              </span>
            </p>
            <p>
              <strong className="text-indigo-500 text-sm font-bold">
                Número OneSignal
              </strong>
              <br />

              <span className="text-xs bg-yellow-200 p-1 rounded-md">
                {prefecture.configurations?.oneSignalProjectNumber
                  ? prefecture.configurations?.oneSignalProjectNumber
                  : "Não Cadastrado"}
              </span>
            </p>
          </div>
          <a
            href={prefecture.urlAccess}
            target="_blank"
            className="w-full rounded-md border border-indigo-500 p-2 block mt-24 text-center font-lg text-indigo-800 font-medium hover:bg-indigo-900 hover:text-white"
          >
            Acessar Painel
          </a>
        </>
      </Slide>
    </HelmetProvider>
  );
};

export default View;
