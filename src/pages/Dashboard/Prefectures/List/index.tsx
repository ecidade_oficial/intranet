import { PrefectureInterface } from "@/model/Prefecture";
import React, { createElement, useEffect, useState } from "react";
import View from "./view";
import { getAllPrefectures } from "../../../../services/Prefectures";
export default function ListPrefectures() {
  const [prefectures, setPrefectures] = useState<PrefectureInterface[]>([]);
  useEffect(() => {
    async function getPrefectures() {
      let objPrefectures = await getAllPrefectures();

      setPrefectures(objPrefectures);
    }
    getPrefectures();
  }, []);
  console.log(prefectures);
  return createElement(View, { prefectures });
}
