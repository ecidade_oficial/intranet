import React, { createElement, useEffect, useState } from "react";
import View from "./view";
import { useForm } from "react-hook-form";
import Prefecture, { PrefectureInterface } from "@/model/Prefecture";
import { HttpCodes } from "@/helpers/constants/HttpCodes";
import { useHistory, useParams } from "react-router-dom";
import { getPrefecture } from "@/services/Prefectures";
import slugify from "slugify";
export default function EditPrefecture() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
  } = useForm<PrefectureInterface>();
  const history = useHistory();
  const [loadingData, setLoadingData] = useState(false);
  const [loading, setLoading] = useState(false);
  const [prefecture, setPrefecture] = useState();
  const handleGenerateSlug = (): void => {
    let values = getValues();
    let name = values.name;
    setValue("prefixDomain", slugify(name, { replacement: "_", lower: true }));
  };
  let { prefectureId } = useParams<{ prefectureId?: string }>();
  useEffect(() => {
    async function getPrefectureBYId() {
      setLoadingData(true);
      let objPrefectures = await getPrefecture(Number(prefectureId));
      if (!objPrefectures) {
        history.push("/prefeituras");
      }

      setPrefecture(objPrefectures);

      setTimeout(() => {
        setLoadingData(false);
      }, 3000);
    }
    getPrefectureBYId();
  }, []);
  const handlerEdit = async (data: PrefectureInterface): Promise<any> => {
    setLoading(true);
    const newPrefecture = await Prefecture.updatePrefecture(
      Number(prefectureId),
      data
    );
    setLoading(false);
    if (newPrefecture.status === HttpCodes.SUCCESS) {
      history.push("/prefeituras");
    }
  };
  return createElement(View, {
    register,
    errors,
    handleSubmit,
    handlerEdit,
    handleGenerateSlug,
    prefecture,
    loadingData,
    loading,
    setValue,
  });
}
