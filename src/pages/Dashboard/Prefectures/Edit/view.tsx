import React from "react";
import { Helmet, HelmetProvider } from "react-helmet-async";
import {
  DeepMap,
  FieldError,
  UseFormHandleSubmit,
  UseFormRegister,
} from "react-hook-form";

import Prefecture, { PrefectureInterface } from "@/model/Prefecture";
import { Link } from "react-router-dom";
import Input from "@/component/Input";
import Loading from "@/component/Loading";
import Botao from "@/component/Botao";
interface CreatePrefectureProps {
  register: UseFormRegister<PrefectureInterface>;
  errors: DeepMap<Prefecture, FieldError>;
  handleSubmit: UseFormHandleSubmit<PrefectureInterface>;
  handlerEdit: (data: PrefectureInterface) => void;
  handleGenerateSlug: () => void;
  prefecture: any;
  loadingData: boolean;
  loading: boolean;
  setValue: any;
}
const View = ({
  register,
  errors,
  handleSubmit,
  handlerEdit,
  handleGenerateSlug,
  prefecture,
  loadingData,
  loading,
  setValue,
}: CreatePrefectureProps) => {
  console.log(prefecture);
  return (
    <HelmetProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>eCidade Intranet | Prefeitura Novo</title>
      </Helmet>
      <div className="flex justify-between mx-auto px-4 sm:px-6 md:px-8">
        <h1 className="text-2xl font-semibold text-gray-900">
          Nova Prefeitura
        </h1>
      </div>
      {loadingData ? (
        <Loading />
      ) : (
        <form onSubmit={handleSubmit(handlerEdit)} method="POST">
          <div className=" mx-auto px-4 sm:px-6 md:px-8">
            <div className="py-4">
              <div className="bg-white shadow px-4 py-5 sm:rounded-lg sm:p-6">
                <div className="pb-5">
                  <div className="py-4">
                    <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                      <div className="sm:col-span-1  md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Prefeitura"
                          inputName="name"
                          messageError="Informe o Nome da Prefeitura"
                          required={true}
                          value={setValue("name", prefecture?.name)}
                          onKeyUp={handleGenerateSlug}
                        />
                      </div>
                      <div className="sm:col-span-1  md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Dominio"
                          inputName="prefixDomain"
                          messageError="Informe o Dominio"
                          required={true}
                          value={setValue("prefixDomain", prefecture?.prefix)}
                        />
                      </div>
                      <div className="sm:col-span-2 md:col-span-3 lg:col-span-2  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="OneSignal APP ID"
                          inputName="oneSignalAppId"
                          required={false}
                          value={setValue(
                            "oneSignalAppId",
                            prefecture?.onesignalAppId
                          )}
                        />
                      </div>
                      <div className="sm:col-span-2   md:col-span-3 lg:col-span-2 relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="OneSignal API KEY"
                          inputName="oneSignalApiKey"
                          required={false}
                          value={setValue(
                            "oneSignalApiKey",
                            prefecture?.onesignalApiId
                          )}
                        />
                      </div>
                    </div>
                    <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                      <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="OneSignal Project Number"
                          inputName="configurations.oneSignalProjectNumber"
                          required={false}
                          value={setValue(
                            "configurations.oneSignalProjectNumber",
                            prefecture?.configurations?.oneSignalProjectNumber
                          )}
                        />
                      </div>
                      <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Chave Firebase"
                          inputName="configurations.firebaseServerKey"
                          required={false}
                          value={setValue(
                            "configurations.firebaseServerKey",
                            prefecture?.configurations?.firebaseServerKey
                          )}
                        />
                      </div>
                      <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="URL Loja Android"
                          inputName="urlAndroid"
                          required={false}
                          value={setValue("urlAndroid", prefecture?.urlAndroid)}
                        />
                      </div>
                      <div className="sm:col-span-2 md:col-span-3 lg:col-span-2 relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="URL Loja IOS"
                          inputName="urlIos"
                          required={false}
                          value={setValue("urlIos", prefecture?.urlIos)}
                        />
                      </div>
                      <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Apple Login"
                          inputName="configurations.appleLogin"
                          required={false}
                          value={setValue(
                            "configurations.appleLogin",
                            prefecture?.configurations?.appleLogin
                          )}
                        />
                      </div>
                      <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Apple Senha"
                          inputName="configurations.applePassword"
                          required={false}
                          value={setValue(
                            "configurations.applePassword",
                            prefecture?.configurations?.applePassword
                          )}
                        />
                      </div>
                      <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Código HG"
                          inputName="hgBrasilName"
                          required={false}
                          value={setValue(
                            "hgBrasilName",
                            prefecture?.hgBrasilName
                          )}
                        />
                        <Link
                          to={{
                            pathname:
                              "https://console.hgbrasil.com/documentation/weather/tools",
                          }}
                          title="Obter Código HD"
                          target="_blank"
                          className="underline text-gray-800 font-medium"
                        >
                          <small>Obter Código</small>
                        </Link>
                      </div>

                      <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Cor do Modulo"
                          inputName="configurations.backgroundColor"
                          messageError="Informe a cor dos modulos"
                          required={true}
                          value={setValue(
                            "configurations.backgroundColor",
                            prefecture?.configurations?.backgroundColor
                          )}
                        />
                      </div>
                      <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Cor Titulo Modulo"
                          inputName="configurations.titleColor"
                          required={true}
                          messageError="Informe a Cor do Titulo"
                          value={setValue(
                            "configurations.titleColor",
                            prefecture?.configurations?.titleColor
                          )}
                        />
                      </div>
                      <div className="sm:col-span-1 md:col-span-3 lg:col-span-1  relative">
                        <Input
                          register={register}
                          errors={errors}
                          label="Cor Texto Modulo"
                          inputName="configurations.textColor"
                          required={true}
                          messageError="Informe a Cor do Modulo"
                          value={setValue(
                            "configurations.textColor",
                            prefecture?.configurations?.textColor
                          )}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex justify-between mx-auto px-4 sm:px-6 md:px-8">
            <Link
              to="/prefeituras"
              className="inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Voltar
            </Link>
            <Botao
              loading={loading}
              titleButton="Salvar"
              titleLoading="Salvando"
            />
          </div>
        </form>
      )}
    </HelmetProvider>
  );
};
export default View;
