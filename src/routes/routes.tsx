import React, { Component } from "react";
import {
  BrowserRouter,
  Redirect,
  Route,
  RouteProps,
  Switch,
} from "react-router-dom";
import { useAuth } from "../contexts/auth";
import Login from "../pages/Login";

import Dashboard from "../pages/Dashboard";
import ListPrefectures from "../pages/Dashboard/Prefectures/List/index";
import CreatePrefecture from "../pages/Dashboard/Prefectures/Create/index";
import EditPrefecture from "../pages/Dashboard/Prefectures/Edit/index";
import TemplateDashboard from "../component/TemplateDashboard";

interface PrivateRouteProps extends RouteProps {
  // tslint:disable-next-line:no-any
  component: any;
  signed: boolean;
}

// @ts-ignore
const PrivateRoute = ({ component: Component, ...otherProps }) => {
  const { signed, isLoading } = useAuth();
  console.log(signed, isLoading);
  return (
    <Route
      {...otherProps}
      render={(props) =>
        !isLoading ? (
          signed ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={otherProps.redirectTo ? otherProps.redirectTo : "/"}
            />
          )
        ) : (
          <h1>Carregando</h1>
        )
      }
    />
  );
};

const Routes = () => {
  const { signed } = useAuth();

  // @ts-ignore
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />

        <TemplateDashboard>
          <PrivateRoute exact path="/painel" component={Dashboard} />
          <PrivateRoute exact path="/prefeituras" component={ListPrefectures} />
          <PrivateRoute
            exact
            path="/prefeituras/novo"
            component={CreatePrefecture}
          />
          <PrivateRoute
            exact
            path="/prefeituras/:prefectureId/editar"
            component={EditPrefecture}
          />
        </TemplateDashboard>
      </Switch>
    </BrowserRouter>
  );
};
export default Routes;
