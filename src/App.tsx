import React from "react";
import { AuthProvider } from "./contexts/auth";

import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";
import Routes from "./routes/routes";
const App = () => (
  <AuthProvider>
    <Routes />
    <ToastContainer />
  </AuthProvider>
);

export default App;
