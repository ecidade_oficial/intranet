
export const URL = {
    baseUrl: import.meta.env.VITE_BASE_API,
    auth: {
        csrfToke: '/sanctum/csrf-cookie',
        login: '/login',
        logout: '/logout'
    },
    user: {
        getUser: '/user'
    },
    prefecture: '/prefectures'

}