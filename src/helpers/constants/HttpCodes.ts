export class HttpCodes {
    static get SUCCESS() {
      return 200;
    }
    static get CREATED() {
      return 201;
    }
    static get BAD_REQUEST() {
      return 400;
    }
  
    static get UNAUTHORIZED() {
      return 401;
    }
  
    static get UNAUTHORIZED_LOGOUT() {
      return 418;
    }
  
    static get FORBIDDEN() {
      return 403;
    }
  
    static get NOT_FOUND() {
      return 404;
    }
  
    static get AWAITING_LIBERATION() {
      return 419;
    }
  
    static get APPLICATION_EXCEPTION() {
      return 420;
    }
    static get UNPROCESSABLE_ENTITY() {
      return 422;
    }
    static get INTERNAL_SERVER_ERROR() {
      return 500;
    }
  }
  