export const cpfMask = (value: string) => {
    return value
        .replace(/\D/g, "")
        .replace(/(\d{3})(\d)/, "$1.$2")
        .replace(/(\d{3})(\d)/, "$1.$2")
        .replace(/(\d{3})(\d{1,2})/, "$1-$2")
        .replace(/(-\d{2})\d+?$/, "$1");
};

export const phoneMask = (value: string): string => {
    let textAdjust = value.replace(/\-/g, '');
    const isCellphone = textAdjust.length === 9;

    if (isCellphone) {
        return `${textAdjust.slice(0, 5)}-${textAdjust.slice(5, 10)}`
    }
    return `${textAdjust.slice(0, 4)}-${textAdjust.slice(4, 8)}`
}
