import { create, update } from "@/services/Prefectures";

export interface Configurations {
    firebaseServerKey?: string
    appleLogin?: string
    applePassword?: string
    oneSignalProjectNumber?: string
    backgroundColor?: string
    titleColor?: string
    textColor?: string

}
export interface User {
    email: string
    password: string
}
export interface PrefectureInterface {
    id?: number
    logo?: string
    name: string
    prefixDomain: string
    prefix?: string
    status?: string
    hgBrasilName: string
    oneSignalAppId?: string
    oneSignalApiKey?: string
    onesignalApiId?: string
    country?: string
    urlAndroid?: string
    urlAccess?: string
    urlIos?: string
    configurations?: Configurations
    user?: User
}

export default class Prefecture {
    static async createPrefecture(newPrefecture: PrefectureInterface): Promise<any> {
        return create(newPrefecture);

    }
    static async updatePrefecture(prefectureId: number, prefecture: PrefectureInterface): Promise<any> {
        return update(prefecture, prefectureId);
    }
}