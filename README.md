# imobiliaria-front

# Itens Pendentes

- [ ] Incluir notificação de cadastro
- [ ] Incluir notificação de login
- [ ] Indicação de loading quando clicar no botão
- [ ] Notificação de falha no cadastro
- [ ] Notificação de falha no login
- [ ] Revisar regra do login para casos de falha
